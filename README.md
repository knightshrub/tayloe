# Tayloe quadrature sampling detector

An implementation of the Quadrature sampling detector introduced by Dan Tayloe.
The PCB can be stacked on-top of an STM32F401RE-Nucleo board.


## Schematic

![Schematic](tayloe.svg)

# License

This project is open hardware. The [KiCAD](https://www.kicad.org/) design files are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International (CC-BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) License.

[![CC-BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)
